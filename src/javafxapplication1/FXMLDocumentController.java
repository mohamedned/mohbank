
package javafxapplication1;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author Mohamed Ali Abdi
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private AnchorPane rootPane;
    
    @FXML
    private Button button;
     
    @FXML
    private Button buton1; 
    
    @FXML
    private Button button2;
    
    
    /**
     *
     * @param event
     */
    @FXML
    public void handleButtonAction(ActionEvent event) {
        try{
        AnchorPane pane = FXMLLoader.load(getClass().getResource("FXML.fxml"));
        rootPane.getChildren().setAll(pane);
        } catch(Exception e) {
            System.out.print(" Can not load new window ");
        }
    }
    
     @FXML
    void handleButtonActionB(ActionEvent event) throws IOException {
        AnchorPane pane = FXMLLoader.load(getClass().getResource("SaldoInfo.fxml"));
        rootPane.getChildren().setAll(pane);
    } 
    
    @FXML
    void handleButtonActionC(ActionEvent event) throws IOException {
        AnchorPane pane = FXMLLoader.load(getClass().getResource("PincodeWijzigen.fxml"));
        rootPane.getChildren().setAll(pane);
    }
    
    @FXML
    void handleButtonActionD(ActionEvent event) throws IOException {
        AnchorPane pane = FXMLLoader.load(getClass().getResource("pincodeInvoer.fxml"));
        rootPane.getChildren().setAll(pane);
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       
    }    
    
}
