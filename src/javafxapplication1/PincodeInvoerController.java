/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication1;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class PincodeInvoerController extends BaseController implements Initializable {
    final private int MIN_PINCODE_SIZE = 4;
    final private int MAX_PINCODE_SIZE = 4;
    @FXML
    private AnchorPane rootPane;
   
    @FXML
    private Label labelPincode;
    
    
    @FXML
    private PasswordField pincodeField;
 
        
    @FXML
    private void onPincodeAction(ActionEvent event) 
    {
        System.out.println("[PASSWORDFIELD] onPincodeAction");
        
        if(pincodeField.getLength() < MIN_PINCODE_SIZE){
             System.out.println("[PASSWORDFIELD] Entered less than " + MIN_PINCODE_SIZE + " chars.");
             labelPincode.setText("Entered less than " + MIN_PINCODE_SIZE + " numbers");
             return;
        }
        if(!pincodeField.getText().equals("1231")){
            System.out.println("[PASSWORDFIELD] Entered wrong pincode.");
            labelPincode.setText("Entered wrong pincode");
            return;
        }
     // showView(getClass().getResource("WelkomSchermController.fxml"));
        try {
            AnchorPane pane = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
            rootPane.getChildren().setAll(pane);
        } catch (IOException ex) {
            Logger.getLogger(PincodeInvoerController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void terugAction(ActionEvent event) throws IOException {
        AnchorPane pane = FXMLLoader.load(getClass().getResource("Welkom.fxml"));
        rootPane.getChildren().setAll(pane);
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       //pincodeField.setOnAction(this::onPincodeAction);
    }    
    
}
