
package javafxapplication1;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Mohamed Ali Abdi
 */
public class SaldoInfoController implements Initializable {
    @FXML
    private AnchorPane rootPane;
    
    @FXML
    private void terugAction(ActionEvent event) {
       
        try {
            AnchorPane pane =FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        rootPane.getChildren().setAll(pane);
        } catch (IOException ex) {
            Logger.getLogger(SaldoInfoController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
