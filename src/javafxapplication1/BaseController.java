package javafxapplication1;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;

public abstract class BaseController
{
    protected Stage stage;

    public void setStage(Stage stage)
    {
        this.stage = stage;
    }

    public Stage getStage()
    {
        return stage;
    }

    public void showView(URL view)
    {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(view);
            Parent root = (Parent) fxmlLoader.load();

            stage.setScene(new Scene(root));

            BaseController controller = (BaseController) fxmlLoader.getController();
            controller.setStage(stage);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}